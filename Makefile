# Usage:
# * make clean
# * make all
# * make upload
#
# The following need to be defined:
# ARDUINO_MAKEFILE
# MONITOR_PORT

BOARD_TAG = nano
BOARD_SUB = atmega328
ARDUINO_LIBS = Servo

include $(ARDUINO_MAKEFILE)
