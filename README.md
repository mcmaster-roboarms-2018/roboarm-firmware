# Roboarm Arduino Firmware
## Requirements
This project needs [arduino-mk](https://github.com/sudar/Arduino-Makefile)

## Building
You need to define:
* `ARDUINO_MAKEFILE`
    * The path to arduino-mk's `Arduino.mk`
* `MONITOR_PORT`
    * The usb port that the arduino is connected to (`/dev/ttyACM0`)

`make clean`
`make all`
`make upload`

