#include <Arduino.h>
#include <HardwareSerial.h>
#include "Servo.h"

#define BAUDRATE (115200)
#define SERVO_MIN_ANGLE (-90)
#define SERVO_MIN_MS (700)
#define SERVO_MAX_ANGLE (90)
#define SERVO_MAX_MS (2400)

#define NUM_JOINTS (5)

#define CMD_WRITE_ANGLES (0x0)
#define CMD_READ_ANGLES (0x1)
#define CMD_SYN (0x2)
#define CMD_ACK (0x3)
#define CMD_ERR (0x4)
#define CMD_GET_NUM_JOINTS (0x5)

uint8_t in_byte;

float joint_angles[NUM_JOINTS];
union FloatConvert {
    float f;
    uint8_t b[4];
} float_converter;

Servo joints[NUM_JOINTS];
uint8_t joint_pins[] = {3, 5, 6, 9, 10, 11};
bool update_angles = true;
char buf[32];

uint16_t angle_to_us(float angle) {
    return (angle - SERVO_MIN_ANGLE) * (SERVO_MAX_MS - SERVO_MIN_MS) / (SERVO_MAX_ANGLE - SERVO_MIN_ANGLE) + SERVO_MIN_MS;
}

void setup() {
    Serial.begin(BAUDRATE);
    for (uint8_t i = 0;i < NUM_JOINTS;i++) {
        joints[i].attach(joint_pins[i], SERVO_MIN_MS, SERVO_MAX_MS);
    }
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.println("!");
}

void loop() {
    if (Serial.available() > 0) {
        in_byte = Serial.read(); // read the incoming byte

        switch (in_byte) { // Any command should be acknowledged with the same command
            case CMD_SYN: // B
                Serial.write(CMD_SYN);
                Serial.write(CMD_ACK);
                Serial.flush();
                digitalWrite(LED_BUILTIN, LOW);
                break;

            case CMD_WRITE_ANGLES: // B, f*num
                Serial.write(CMD_WRITE_ANGLES);
                Serial.flush();

                while (Serial.available() < (signed)(sizeof(float) * NUM_JOINTS))
                    ; // Busy wait for serial buffer

                for (uint8_t i = 0;i < NUM_JOINTS;i++) {
                    // Read in the float to the union
                    float_converter.b[0] = Serial.read();
                    float_converter.b[1] = Serial.read();
                    float_converter.b[2] = Serial.read();
                    float_converter.b[3] = Serial.read();
                    joint_angles[i] = float_converter.f; // Store the union result
                }
                update_angles = true;
                break;

            case CMD_READ_ANGLES: // B
                Serial.write(CMD_READ_ANGLES);
                Serial.flush();

                for (uint8_t i = 0;i < NUM_JOINTS;i++) {
                    float_converter.f = joint_angles[i];
                    Serial.write(float_converter.b[0]);
                    Serial.write(float_converter.b[1]);
                    Serial.write(float_converter.b[2]);
                    Serial.write(float_converter.b[3]);
                }
                Serial.flush();
                break;

            case CMD_GET_NUM_JOINTS: // B
                Serial.write(CMD_GET_NUM_JOINTS);
                Serial.flush();

                Serial.write(NUM_JOINTS);
                Serial.flush();
                break;

            default:
                Serial.write(CMD_ERR);
                sprintf(buf, "CMD Error: %i", in_byte);
                Serial.println(buf);
                Serial.flush();
                break;
        }
    }

    if (update_angles) {
        for (uint8_t i = 0;i < NUM_JOINTS;i++) {
            joints[i].writeMicroseconds(angle_to_us(joint_angles[i]));
        }
        update_angles = false;
    }
}
